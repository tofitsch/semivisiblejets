import ROOT
import uproot
import math
import itertools
import numpy as np
from more_itertools import sort_together
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from sklearn.decomposition import PCA

ROOT.gInterpreter.Declare('R__ADD_INCLUDE_PATH(Delphes-3.5.0);')
ROOT.gInterpreter.Declare('R__ADD_INCLUDE_PATH(Delphes-3.5.0/external);')
ROOT.gInterpreter.Declare('R__LOAD_LIBRARY(Delphes-3.5.0/libDelphes);')
ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.SetStyle('ATLAS')
ROOT.gStyle.SetOptStat(0)

# change from here ############################

jet_image_mode = True # else: variable plotting mode

in_tree_name = 'Delphes'

out_dir = './plots'

channels = { #name_in_legend    #colour          #input_root_files
#             'bkg QCD':         [ROOT.kBlack,    [
#                                                  'samples_from_cesare/Backgrounds/QCD_heavy_flavour_ptHat_200GeV.root',
#                                                  'samples_from_cesare/Backgrounds/QCD_light_flavour_ptHat_200GeV.root',
#                                                  'samples_from_cesare/Backgrounds/QCD_c_flavour_ptHat_200GeV.root',
#                                                 ]],

#             't-channel r_{inv} = 0.0': [ROOT.kRed+2,    ['samples_from_cesare/model_1/t_channel_pair_production/HL_LHC_reco/t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_sqrts_13TeV.root']],
#             't-channel r_{inv} = 0.3': [ROOT.kOrange+2, ['samples_from_cesare/model_1/t_channel_pair_production/HL_LHC_reco/t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_3_sqrts_13TeV.root']],
#             't-channel r_{inv} = 0.5': [ROOT.kBlue+2,   ['samples_from_cesare/model_1/t_channel_pair_production/HL_LHC_reco/t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_5_sqrts_13TeV.root']],
#             't-channel r_{inv} = 0.7': [ROOT.kGreen+2,  ['samples_from_cesare/model_1/t_channel_pair_production/HL_LHC_reco/t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_7_sqrts_13TeV.root']],

#             'SVJ(CMS) M_{Z^{\prime}} = 3 TeV': [ROOT.kViolet,  ['samples_from_cesare/standard/r_inv_0_3_MZp_3000GeV.root']],

#             'SVJtau M_{Z^{\prime}} = 3 TeV #alpha = 0.1': [ROOT.kRed,    ['samples_from_cesare/model_2/mg5_and_py8/SVJL_s_channel_taus_MZp_3TeV_alpha_0_1_rinv_0_3_mpi_8GeV_lambdaHV_10GeV.root']],
#             'SVJtau M_{Z^{\prime}} = 3 TeV #alpha = 0.3': [ROOT.kOrange, ['samples_from_cesare/model_2/mg5_and_py8/SVJL_s_channel_taus_MZp_3TeV_alpha_0_3_rinv_0_3_mpi_8GeV_lambdaHV_10GeV.root']],
#             'SVJtau M_{Z^{\prime}} = 3 TeV #alpha = 0.5': [ROOT.kBlue,   ['samples_from_cesare/model_2/mg5_and_py8/SVJL_s_channel_taus_MZp_3TeV_alpha_0_5_rinv_0_3_mpi_8GeV_lambdaHV_10GeV.root']],
#             'SVJtau M_{Z^{\prime}} = 3 TeV #alpha = 0.6': [ROOT.kGreen,  ['samples_from_cesare/model_2/mg5_and_py8/SVJL_s_channel_taus_MZp_3TeV_alpha_0_6_rinv_0_3_mpi_8GeV_lambdaHV_10GeV.root']],
#             'SVJtau M_{Z^{\prime}} = 3 TeV #alpha = 1.0': [ROOT.kYellow,  ['samples_from_cesare/model_2/mg5_and_py8/SVJL_s_channel_taus_MZp_3TeV_alpha_1_rinv_0_3.root']],

#             'SVJtau(py8only) M_{Z^{\prime}} = 3 TeV #alpha = 0.1': [ROOT.kRed,    ['samples_from_cesare/model_2/svjtaus_onlypy8/alpha_0_1_r_inv_0_3_MZp_3000GeV.root']],
#             'SVJtau(py8only) M_{Z^{\prime}} = 3 TeV #alpha = 0.3': [ROOT.kOrange, ['samples_from_cesare/model_2/svjtaus_onlypy8/alpha_0_3_r_inv_0_3_MZp_3000GeV.root']],
             'SVJtau(py8only) M_{Z^{\prime}} = 3 TeV #alpha = 0.5': [ROOT.kBlue,   ['samples_from_cesare/model_2/svjtaus_onlypy8/alpha_0_5_r_inv_0_3_MZp_3000GeV.root']],
#             'SVJtau(py8only) M_{Z^{\prime}} = 3 TeV #alpha = 0.6': [ROOT.kGreen,  ['samples_from_cesare/model_2/svjtaus_onlypy8/alpha_0_6_r_inv_0_3_MZp_3000GeV.root']],
           }

# branches to load from files:
primary_vars = [
                 'ParticleFlowJet08/ParticleFlowJet08.Constituents',
                 'ParticleFlowJet08/ParticleFlowJet08.Particles',
                 'ParticleFlowJet08/ParticleFlowJet08.PT',
                 'ParticleFlowJet08/ParticleFlowJet08.Eta',
                 'ParticleFlowJet08/ParticleFlowJet08.Phi',
                 'EFlowTrack/EFlowTrack.fUniqueID',
                 'EFlowTrack/EFlowTrack.PID',
                 'EFlowTrack/EFlowTrack.PT',
                 'EFlowTrack/EFlowTrack.Eta',
                 'EFlowTrack/EFlowTrack.Phi',
                 'EFlowPhoton/EFlowPhoton.fUniqueID',
                 'EFlowPhoton/EFlowPhoton.ET',
                 'EFlowPhoton/EFlowPhoton.Eta',
                 'EFlowPhoton/EFlowPhoton.Phi',
                 'EFlowNeutralHadron/EFlowNeutralHadron.fUniqueID',
                 'EFlowNeutralHadron/EFlowNeutralHadron.ET',
                 'EFlowNeutralHadron/EFlowNeutralHadron.Eta',
                 'EFlowNeutralHadron/EFlowNeutralHadron.Phi',
                 'MissingET/MissingET.MET',
                 'MissingET/MissingET.Phi',
                 'Particle/Particle.PID',
                 'Particle/Particle.PT',
                 'Particle/Particle.Eta',
                 'Particle/Particle.Phi',
                 'Particle/Particle.Status',
               ] if jet_image_mode else [
                 '<DelphesP4>ParticleFlowJet04',
                 '<DelphesP4>ParticleFlowJet08',
                 'ParticleFlowJet04/ParticleFlowJet04.PT',
                 'ParticleFlowJet08/ParticleFlowJet08.PT',
                 'Electron/Electron.PT',
                 'Muon/Muon.PT',
                 'ParticleFlowJet04/ParticleFlowJet04.Eta',
                 'ParticleFlowJet08/ParticleFlowJet08.Eta',
                 'Electron/Electron.Eta',
                 'Muon/Muon.Eta',
                 'MissingET/MissingET.MET',
                 'MissingET/MissingET.Phi',
                 'EFlowTrack/EFlowTrack.PID',
                 'EFlowTrack/EFlowTrack.PT',
                 'EFlowTrack/EFlowTrack.Eta',
                 'EFlowTrack/EFlowTrack.Phi',
                 'Particle/Particle.PID',
                 'Particle/Particle.PT',
                 'Particle/Particle.Eta',
                 'Particle/Particle.Phi',
                 'Particle/Particle.Status',
                 'Photon/Photon.PT',
                 'Photon/Photon.Eta',
                 'Photon/Photon.Phi',
               ]

# variables to plot (must be either in 'primary_vars' or defined in 'calculate_secondary_vars()' below)
plotting_vars = { #1d plots
                  #branch_name                  #axis_label                                         #binning
                  'ParticleFlowJet04_N':        ['N(j_{0.4, pflow})',                               (10, 0, 10)],
                  'ParticleFlowJet08_N':        ['N(j_{0.8, pflow})',                               (10, 0, 10)],
                  'Electron_N':                 ['N(e)',                                            (10, 0, 10)],
                  'Muon_N':                     ['N(#mu)',                                          (10, 0, 10)],
                  'ParticleFlowJet04_pT_lead':  ['p_{T}(j^{lead}_{0.4, pflow}) [GeV]',              (50, 0, 1000.)],
                  'ParticleFlowJet08_pT_lead':  ['p_{T}(j^{lead}_{0.8, pflow}) [GeV]',              (50, 0, 1000.)],
                  'Electron_pT_lead':           ['p_{T}(e^{lead}) [GeV]',                           (20, 0, 200.)],
                  'Muon_pT_lead':               ['p_{T}(#mu^{lead}) [GeV]',                         (20, 0, 200.)],
                  'ParticleFlowJet04_eta_lead': ['#eta(j^{lead}_{0.4, pflow})',                     (50, -5., 5.)],
                  'ParticleFlowJet08_eta_lead': ['#eta(j^{lead}_{0.8, pflow})',                     (50, -5., 5.)],
                  'Electron_eta_lead':          ['#eta(e^{lead})',                                  (50, -5., 5.)],
                  'Muon_eta_lead':              ['#eta(#mu^{lead})',                                (50, -5., 5.)],
                  'MissingET/MissingET.MET':    ['E_{T}^{miss}',                                    (50, 0, 1000)],
                  'raw_e_lead_iso':             ['inter-isolation(e^{lead}_{raw})',                 (50, 0., 1.)],
                  'raw_e_avg_iso':              ['average inter-isolation(e)',                      (50, 0., 1.)],
                  'raw_mu_lead_iso':            ['inter-isolation(#mu^{lead}_{raw})',               (50, 0., 1.)],
                  'raw_mu_avg_iso':             ['average inter-isolation(#mu)',                    (50, 0., 1.)],
                  'raw_tau_lead_iso':           ['inter-isolation(#tau^{lead}_{raw})',              (50, 0., 1.)],
                  'raw_tau_avg_iso':            ['average inter-isolation(#tau)',                   (50, 0., 1.)],
                  'raw_tau_lead_nom_iso':       ['nominal inter-isolation(#tau^{lead}_{raw})',      (50, 0., 1.)],
                  'raw_tau_avg_nom_iso':        ['nominal average inter-isolation(#tau)',           (50, 0., 1.)],
                  'raw_e_n':                    ['N(e)',                                            (10, 0., 10)],
                  'raw_mu_n':                   ['N(#mu)',                                          (10, 0., 10)],
                  'raw_tau_n':                  ['N(#tau)',                                         (50, 0., 100)],
                  'raw_e_lead_pT':              ['p_{T}(e^{lead}) [GeV]',                           (100, 0., 200.)],
                  'raw_e_lead_eta':             ['#eta(e^{lead})',                                  (50, -5., 5.)],
                  'raw_mu_lead_pT':             ['p_{T}(#mu^{lead}) [GeV]',                         (100, 0., 200.)],
                  'raw_mu_lead_eta':            ['#eta(#mu^{lead})',                                (50, -5., 5.)],
                  'raw_tau_lead_pT':            ['p_{T}(#tau^{lead}) [GeV]',                        (100, 0., 200.)],
                  'raw_tau_lead_eta':           ['#eta(#tau^{lead})',                               (50, -5., 5.)],
                  'n_taus_in_lead_jet':         ['number of #tau in j^{lead}_{0.8, pflow}',         (15, 0, 15)],
                  'n_taus_in_sublead_jet':      ['number of #tau in j^{sublead}_{0.8, pflow}',      (15, 0, 15)],
                  'n_taus_in_3rd_jet':          ['number of #tau in j^{3rd}_{0.8, pflow}',          (15, 0, 15)],
                  'n_taus_in_4th_jet':          ['number of #tau in j^{4th}_{0.8, pflow}',          (15, 0, 15)],
                  'n_pi0s_in_lead_jet':         ['number of #pi^{0} in j^{lead}_{0.8, pflow}',      (100, 0, 100)],
                  'n_pi0s_in_sublead_jet':      ['number of #pi^{0} in j^{sublead}_{0.8, pflow}',   (100, 0, 100)],
                  'n_pi0s_in_3rd_jet':          ['number of #pi^{0} in j^{3rd}_{0.8, pflow}',       (100, 0, 100)],
                  'n_pi0s_in_4th_jet':          ['number of #pi^{0} in j^{4th}_{0.8, pflow}',       (100, 0, 100)],
                  'n_pipms_in_lead_jet':        ['number of #pi^{#pm} in j^{lead}_{0.8, pflow}',    (150, 0, 150)],
                  'n_pipms_in_sublead_jet':     ['number of #pi^{#pm} in j^{sublead}_{0.8, pflow}', (150, 0, 150)],
                  'n_pipms_in_3rd_jet':         ['number of #pi^{#pm} in j^{3rd}_{0.8, pflow}',     (150, 0, 150)],
                  'n_pipms_in_4th_jet':         ['number of #pi^{#pm} in j^{4th}_{0.8, pflow}',     (150, 0, 150)],
                  'n_photons_in_lead_jet':      ['number of #gamma in j^{lead}_{0.8, pflow}',       (150, 0, 150)],
                  'n_photons_in_sublead_jet':   ['number of #gamma in j^{sublead}_{0.8, pflow}',    (150, 0, 150)],
                  'n_photons_in_3rd_jet':       ['number of #gamma in j^{3rd}_{0.8, pflow}',        (150, 0, 150)],
                  'n_photons_in_4th_jet':       ['number of #gamma in j^{4th}_{0.8, pflow}',        (150, 0, 150)],
                  'n_es_in_lead_jet':         ['number of e in j^{lead}_{0.8, pflow}',         (15, 0, 15)],
                  'n_es_in_sublead_jet':      ['number of e in j^{sublead}_{0.8, pflow}',      (15, 0, 15)],
                  'n_es_in_3rd_jet':          ['number of e in j^{3rd}_{0.8, pflow}',          (15, 0, 15)],
                  'n_es_in_4th_jet':          ['number of e in j^{4th}_{0.8, pflow}',          (15, 0, 15)],
                  'n_mus_in_lead_jet':         ['number of #mu in j^{lead}_{0.8, pflow}',         (15, 0, 15)],
                  'n_mus_in_sublead_jet':      ['number of #mu in j^{sublead}_{0.8, pflow}',      (15, 0, 15)],
                  'n_mus_in_3rd_jet':          ['number of #mu in j^{3rd}_{0.8, pflow}',          (15, 0, 15)],
                  'n_mus_in_4th_jet':          ['number of #mu in j^{4th}_{0.8, pflow}',          (15, 0, 15)],
                  'mjj_ak4':                   ['m(j^{lead}_{0.4, pflow},j^{sublead}_{0.4, pflow}) [GeV]',                (100, 0., 3500.)],
                  'mjjT_ak4':                  ['m_{T}(j^{lead}_{0.4, pflow},j^{sublead}_{0.4, pflow}) [GeV]',                (100, 0., 3500.)],
                  'mjj_ak8':                   ['m(j^{lead}_{0.8, pflow},j^{sublead}_{0.8, pflow}) [GeV]',                (100, 0., 3500.)],
                  'mjjT_ak8':                  ['m_{T}(j^{lead}_{0.8, pflow},j^{sublead}_{0.8, pflow}) [GeV]',                (100, 0., 3500.)],
                  'pt_frac_from_photons_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma',       (50, 0., 1.)],
                  'pt_frac_from_pi0s_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from #pi^{0}',       (50, 0., 1.)],
                  'pt_frac_from_pipms_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from #pi^{#pm}',       (50, 0., 1.)],
                  'pt_frac_from_taus_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from #tau',       (50, 0., 1.)],
                  'pt_frac_from_mus_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from #mu',       (50, 0., 1.)],
                  'pt_frac_from_es_in_lead_jet':      ['fraction of p_{T} in j^{lead}_{0.8, pflow} from e',       (50, 0., 1.)],
                  'deltaPhi_met_ak4_lead':           ['#Delta#Phi(E_{T}^{miss}, j^{lead}_{0.4, pflow})',           (40, -4., 4.)],
                  'deltaPhi_met_ak4_sublead':           ['#Delta#Phi(E_{T}^{miss}, j^{sublead}_{0.4, pflow})',           (40, -4., 4.)],
                  'deltaPhi_met_ak8_lead':           ['#Delta#Phi(E_{T}^{miss}, j^{lead}_{0.8, pflow})',           (40, -4., 4.)],
                  'deltaPhi_met_ak8_sublead':           ['#Delta#Phi(E_{T}^{miss}, j^{sublead}_{0.8, pflow})',           (40, -4., 4.)],
                  #2d plots
                  #branch names (separated by space)             # x axis label                                  # y axis label                                   #binning
                  'n_pi0s_in_lead_jet n_pipms_in_lead_jet':    [('number of #pi^{0} in j^{lead}_{0.8, pflow}',   'number of #pi^{#pm} in j^{lead}_{0.8, pflow}'), (100, 0, 100, 150, 0, 150)],
                  'n_pi0s_in_lead_jet n_photons_in_lead_jet':  [('number of #pi^{0} in j^{lead}_{0.8, pflow}',   'number of #gamma in j^{lead}_{0.8, pflow}'),    (100, 0, 100, 150, 0, 150)],
                  'n_pipms_in_lead_jet n_photons_in_lead_jet': [('number of #pi^{#pm} in j^{lead}_{0.8, pflow}', 'number of #gamma in j^{lead}_{0.8, pflow}'),    (150, 0, 150, 150, 0, 150)],
                  'pt_frac_from_pi0s_in_lead_jet pt_frac_from_photons_in_lead_jet': [('fraction of p_{T} in j^{lead}_{0.8, pflow} from #pi^{0}', 'fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma'),    (100, 0., 1., 100, 0., 1.)],
                  'pt_frac_from_pipms_in_lead_jet pt_frac_from_photons_in_lead_jet': [('fraction of p_{T} in j^{lead}_{0.8, pflow} from #pi^{#pm}', 'fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma'),    (100, 0., 1., 100, 0., 1.)],
                  'pt_frac_from_taus_in_lead_jet pt_frac_from_photons_in_lead_jet': [('fraction of p_{T} in j^{lead}_{0.8, pflow} from #tau', 'fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma'),    (100, 0., 1., 100, 0., 1.)],
                  'pt_frac_from_mus_in_lead_jet pt_frac_from_photons_in_lead_jet': [('fraction of p_{T} in j^{lead}_{0.8, pflow} from #mu', 'fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma'),    (100, 0., 1., 100, 0., 1.)],
                  'pt_frac_from_es_in_lead_jet pt_frac_from_photons_in_lead_jet': [('fraction of p_{T} in j^{lead}_{0.8, pflow} from e', 'fraction of p_{T} in j^{lead}_{0.8, pflow} from #gamma'),    (100, 0., 1., 100, 0., 1.)],
                }

def phi_subtract(a, b):
 
 c = a - b

 if c < -math.pi: c += 2*math.pi
 if c > math.pi: c -= 2*math.pi

 return c

def get_jet_images(data):

  jet_images = {}

  for channel in data:

    jet_images[channel] = []
    
    for id_c, id_t, pid_t, pt_t, eta_t, phi_t, id_y, pt_y, eta_y, phi_y, id_h, pt_h, eta_h, phi_h, pt_j, eta_j, phi_j, pt_met, phi_met, pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p in zip(data[channel]['ParticleFlowJet08/ParticleFlowJet08.Constituents'], data[channel]['EFlowTrack/EFlowTrack.fUniqueID'], data[channel]['EFlowTrack/EFlowTrack.PID'], data[channel]['EFlowTrack/EFlowTrack.PT'], data[channel]['EFlowTrack/EFlowTrack.Eta'], data[channel]['EFlowTrack/EFlowTrack.Phi'], data[channel]['EFlowPhoton/EFlowPhoton.fUniqueID'], data[channel]['EFlowPhoton/EFlowPhoton.ET'], data[channel]['EFlowPhoton/EFlowPhoton.Eta'], data[channel]['EFlowPhoton/EFlowPhoton.Phi'], data[channel]['EFlowNeutralHadron/EFlowNeutralHadron.fUniqueID'], data[channel]['EFlowNeutralHadron/EFlowNeutralHadron.ET'], data[channel]['EFlowNeutralHadron/EFlowNeutralHadron.Eta'], data[channel]['EFlowNeutralHadron/EFlowNeutralHadron.Phi'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.PT'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.Eta'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.Phi'], data[channel]['MissingET/MissingET.MET'], data[channel]['MissingET/MissingET.Phi'], data[channel]['Particle/Particle.PT'], data[channel]['Particle/Particle.Eta'], data[channel]['Particle/Particle.Phi'], data[channel]['Particle/Particle.PID'], data[channel]['Particle/Particle.Status']):

      phi_met ,= phi_met

      taus = [(pt, eta, phi) for pt, eta, phi, pid, status in zip(pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p) if abs(pid) == 15 and status == 91]
      taus  = sorted(taus, key=lambda x: x[0], reverse=True)

      id_c = id_c[0] if len(id_c) > 0 else []

      constituents = []

      for c in id_c:
        
        index_t = id_t.tolist().index(c) if c in id_t else None
        if index_t != None:
          constituents.append((pt_t[index_t]/pt_j[0], eta_t[index_t], phi_t[index_t], pid_t[index_t]))
          continue

        index_y = id_y.tolist().index(c) if c in id_y else None
        if index_y != None:
          constituents.append((pt_y[index_y], eta_y[index_y], phi_y[index_y], None))
          continue

        index_h = id_h.tolist().index(c) if c in id_h else None
        if index_h != None:
          constituents.append((pt_h[index_h], eta_h[index_h], phi_h[index_h], None))
          continue
     
      jet_image = {}
      jet_image['pt_frac_e'], jet_image['eta_e'], jet_image['phi_e'] = [], [], []
      jet_image['pt_frac_mu'], jet_image['eta_mu'], jet_image['phi_mu'] = [], [], []
      jet_image['pt_frac'], jet_image['eta'], jet_image['phi'] = [], [], []


      for constituent in constituents:
        
        suffix = ''

        if constituent[3] != None: 

          if abs(constituent[3]) == 11: suffix = '_e'
          if abs(constituent[3]) == 13: suffix = '_mu'

        jet_image['pt_frac' + suffix].append((constituent[0] / pt_j[0]))
        jet_image['eta' + suffix].append(constituent[1])
        jet_image['phi' + suffix].append(constituent[2])

      jet_image['met_pt_frac'], jet_image['met_eta'], jet_image['met_phi'] = None, None, None

      jet_image['eta_tau'], jet_image['phi_tau'] = [], []

      for tau in taus:

        jet_image['eta_tau'].append(tau[1])
        jet_image['phi_tau'].append(tau[2])

      if len(pt_j) > 0:

        jet_image['pt_j'] = pt_j[0]
        jet_image['eta_j'] = eta_j[0]
        jet_image['phi_j'] = phi_j[0]

        jet_image['met_pt_frac'] ,= pt_met / pt_j[0]
        jet_image['met_eta'] = 0.
        jet_image['met_phi'] = phi_met

        
      if len(constituents) == 0: jet_image = None
      
      jet_images[channel].append(jet_image)

  return jet_images

def calc_mean_and_pca(jet_image):

  if jet_image == None: return None
 
  cloud = []

  jet_image['mean_eta'], jet_image['mean_phi'] = 0., 0.
  jet_image['weight_for_mean'] = 0.

  for suffix in ['', '_e', '_mu']:

    for i in range(len(jet_image['pt_frac' + suffix])):

      cloud.append([jet_image['eta' + suffix][i], jet_image['phi' + suffix][i]])

      jet_image['mean_eta'] += jet_image['pt_frac' + suffix][i]*jet_image['eta' + suffix][i]
      jet_image['mean_phi'] += jet_image['pt_frac' + suffix][i]*jet_image['phi' + suffix][i]
      jet_image['weight_for_mean'] += jet_image['pt_frac' + suffix][i]

  cloud = np.array(cloud)

  jet_image['pca0_eta'] = 0.
  jet_image['pca0_phi'] = 0.

  if cloud.shape[0] > 1:

    pca = PCA(n_components=2).fit(cloud)

    jet_image['pca0_eta'] = pca.components_[0][0]
    jet_image['pca0_phi'] = pca.components_[0][1]

  if jet_image['weight_for_mean'] != 0.:

    jet_image['mean_eta'] /= jet_image['weight_for_mean']
    jet_image['mean_phi'] /= jet_image['weight_for_mean']

  else:
    
    jet_image['mean_eta'] = 0.
    jet_image['mean_phi'] = 0.

  return jet_image

def reflect_jet_images(jet_images):

  for channel in jet_images:

    for jet_image in jet_images[channel]:

      if jet_image == None: continue
      
      pt_sum_left = 0.
      pt_sum_right = 0.

      for suffix in ['', '_e', '_mu']:

        for i in range(len(jet_image['pt_frac' + suffix])):
          
          pt_i = jet_image['pt_frac' + suffix][i]
          
          if jet_image['eta' + suffix][i] < 0.: pt_sum_left += pt_i
          else: pt_sum_right += pt_i
      
      if pt_sum_right < pt_sum_left:
  
        for suffix in ['', '_e', '_mu', '_tau']:
  
            for i in range(len(jet_image['eta' + suffix])):
    
              jet_image['eta' + suffix][i] = jet_image['eta' + suffix][i] * -1.

        jet_image['met_eta'] = jet_image['met_eta'] * -1.

        jet_image = calc_mean_and_pca(jet_image)

  return jet_images

def translate_jet_images(jet_images, by='mean'): #by: 'mean', 'jetaxis', or pca0

  for channel in jet_images:

    for jet_image in jet_images[channel]:

      if jet_image == None: continue

      # by == 'jetaxis'
      eta_t = jet_image['eta_j']
      phi_t = jet_image['phi_j']

      if by == 'mean':
      
        eta_t = jet_image['mean_eta']
        phi_t = jet_image['mean_phi']

      elif by == 'pca0':
      
        eta_t = jet_image['pca0_eta']
        phi_t = jet_image['pca0_phi']

      for suffix in ['', '_e', '_mu', '_tau']:
        
        for i in range(len(jet_image['eta' + suffix])):

          jet_image['eta' + suffix][i] = jet_image['eta' + suffix][i] - eta_t
          jet_image['phi' + suffix][i] = phi_subtract(jet_image['phi' + suffix][i], phi_t)

      jet_image['met_eta'] = jet_image['met_eta'] - eta_t
      jet_image['met_phi'] = phi_subtract(jet_image['met_phi'], phi_t)

      jet_image['eta_j'] = jet_image['eta_j'] - eta_t
      jet_image['phi_j'] = phi_subtract(jet_image['phi_j'], - phi_t)

    jet_image = calc_mean_and_pca(jet_image)

  return jet_images

def rotate_around_origin(x_old, y_old, angle):
  
  if x_old == None or y_old == None: return None, None

  x_new =  x_old*np.cos(angle) + y_old*np.sin(angle) 
  y_new = -x_old*np.sin(angle) + y_old*np.cos(angle)

  return x_new, y_new

def rotate_jet_images(jet_images, by='mean'): #by: 'mean', 'met', or 'pca0'
  
  for channel in jet_images:
    
    for jet_image in jet_images[channel]:
      
      if jet_image == None: continue

      x = jet_image[by + '_eta'] 
      y = jet_image[by + '_phi']
      
      angle = np.arctan2(y, x) if x != 0. else 0.

      for suffix in ['', '_e', '_mu', '_tau']:
        
        eta_arr, phi_arr = [], []
        
        for eta, phi in zip(jet_image['eta' + suffix], jet_image['phi' + suffix]):
          
          eta_new, phi_new = rotate_around_origin(eta, phi, angle - math.pi/2.)

          eta_arr.append(eta_new)
          phi_arr.append(phi_new)
          
        jet_image['eta' + suffix], jet_image['phi' + suffix] = eta_arr, phi_arr
        
      jet_image['met_eta'], jet_image['met_phi'] = rotate_around_origin(jet_image['met_eta'], jet_image['met_phi'], angle - math.pi/2.)
      jet_image['pca0_eta'], jet_image['pca0_phi'] = rotate_around_origin(jet_image['pca0_eta'], jet_image['pca0_phi'], angle - math.pi/2.)
      jet_image['mean_eta'], jet_image['mean_phi'] = rotate_around_origin(jet_image['mean_eta'], jet_image['mean_phi'], angle - math.pi/2.)

  return jet_images

def plot_jet_images(jet_images, n_images_per_channel):

  for channel in jet_images:
    
    ctr = 0
    
    for jet_image in jet_images[channel]:
      
      if jet_image == None: continue

      fig = plt.figure()
      ax = fig.add_subplot(111)

      scale = 50

      plt.scatter(jet_image['eta'], jet_image['phi'], s=[scale*100**x for x in jet_image['pt_frac']], alpha=0.5, color='black', label='other')
      plt.scatter(jet_image['eta_e'], jet_image['phi_e'], s=[scale*100**x for x in jet_image['pt_frac_e']], alpha=0.5, color='red', label='e')
      plt.scatter(jet_image['eta_mu'], jet_image['phi_mu'], s=[scale*100**x for x in jet_image['pt_frac_mu']], alpha=0.5, color='blue', label='mu')
      plt.scatter(jet_image['eta_tau'], jet_image['phi_tau'], alpha=0.5, color='green', label='tau', marker='x')

      met_line = LineCollection([[(0., 0.), (jet_image['met_eta'], jet_image['met_phi'])]], color='green', label='met')
      mean_line = LineCollection([[(0., 0.), (jet_image['mean_eta'], jet_image['mean_phi'])]], color='orange', label='pt mean')
      pca0_line = LineCollection([[(0., 0.), (jet_image['pca0_eta'], jet_image['pca0_phi'])]], color='violet', label='PCA 0')
      
      plt.gca().add_collection(met_line)
      plt.gca().add_collection(mean_line)
      plt.gca().add_collection(pca0_line)

      plt.xlim((-1., 1.))
      plt.ylim((-1., 1.))

      plt.xlabel('eta')
      plt.ylabel('phi')

      ax.set_aspect('equal', adjustable='box')

      circle = plt.Circle((0., 0.), .8, linewidth=1, fill=False)
      plt.gca().add_patch(circle)

#      plt.legend()

#      plt.show()
      plt.savefig(out_dir + '/jet_image_' + channel.replace('#', '').replace(' = ', '').replace(' ', '_').replace('{', '').replace('}', '').replace('.', 'p') + '_' + str(ctr) + '.png')

      plt.clf()
      fig.clf()

      ctr += 1

      if ctr >= n_images_per_channel: break

def plot_jet_hists(jet_images):

  hist = {}
  hist_lep = {}

  profile_x = {}
  profile_x_lep = {}

  profile_y = {}
  profile_y_lep = {}

  for channel in jet_images:
    
    imgs = [img for img in jet_images[channel] if img != None]

    hist[channel] = ROOT.TH2F(channel + '_jet_hist', ';#eta;#phi;average p_{T} fraction in j^{lead}', 50, -.8, .8, 50, -.8, .8)
    hist_lep[channel] = ROOT.TH2F(channel + '_jet_hist_lep', ';#eta;#phi;average p_{T} fraction from e,#mu in j^{lead}', 50, -.8, .8, 50, -.8, .8)

    profile_x[channel] = ROOT.TH1F(channel + '_jet_profile_x', ';#phi;average p_{T} fraction in j^{lead} (#phi-#eta)', 20, -1.2, 1.2)
    profile_x_lep[channel] = ROOT.TH1F(channel + '_jet_profile_x_lep', ';#phi;average p_{T} fraction from e,#mu in j^{lead}', 20, -1.2, 1.2)

    profile_y[channel] = ROOT.TH1F(channel + '_jet_profile_y', ';#eta;average p_{T} fraction in j^{lead}', 20, -1.2, 1.2)
    profile_y_lep[channel] = ROOT.TH1F(channel + '_jet_profile_y_lep', ';#eta;average p_{T} fraction from e,#mu in j^{lead}', 20, -1.2, 1.2)

    for jet_image in imgs:
      
      eta = jet_image['eta'] + jet_image['eta_e'] + jet_image['eta_mu']
      phi = jet_image['phi'] + jet_image['phi_e'] + jet_image['phi_mu']
      pt = jet_image['pt_frac'] + jet_image['pt_frac_e'] + jet_image['pt_frac_mu']

      eta_lep = jet_image['eta_e'] + jet_image['eta_mu']
      phi_lep = jet_image['phi_e'] + jet_image['phi_mu']
      pt_lep = jet_image['pt_frac_e'] + jet_image['pt_frac_mu']

      for eta_c, phi_c, pt_c in zip(eta, phi, pt):

        hist[channel].Fill(eta_c, phi_c, pt_c)
        profile_x[channel].Fill(phi_c, pt_c)
        profile_y[channel].Fill(eta_c, pt_c)
        
      for eta_c, phi_c, pt_c in zip(eta_lep, phi_lep, pt_lep):

        hist_lep[channel].Fill(eta_c, phi_c, pt_c)
        profile_x_lep[channel].Fill(phi_c, pt_c)
        profile_y_lep[channel].Fill(eta_c, pt_c)

        hist[channel].Fill(eta_c, phi_c, pt_c)
        profile_x[channel].Fill(phi_c, pt_c)
        profile_y[channel].Fill(eta_c, pt_c)

    hist[channel].Scale(1. / len(imgs))
    hist_lep[channel].Scale(1. / len(imgs))

    profile_x[channel].Scale(1. / len(imgs))
    profile_x_lep[channel].Scale(1. / len(imgs))

    profile_y[channel].Scale(1. / len(imgs))
    profile_y_lep[channel].Scale(1. / len(imgs))
    
#    hist[channel].SetMinimum(1e-5)
#    hist[channel].SetMaximum(1.)
#
#    hist_lep[channel].SetMinimum(1e-5)
#    hist_lep[channel].SetMaximum(1.)
#
#    profile_x[channel].SetMinimum(1e-3)
#    profile_x[channel].SetMaximum(2e-1)
#
#    profile_y[channel].SetMinimum(1e-3)
#    profile_y[channel].SetMaximum(2e-1)
#
#    profile_x_lep[channel].SetMinimum(1e-6)
#    profile_x_lep[channel].SetMaximum(2e-4)
#
#    profile_y_lep[channel].SetMinimum(1e-6)
#    profile_y_lep[channel].SetMaximum(2e-4)

    can = ROOT.TCanvas(channel + '_jet_hist', '', 1200, 1000)

    can.SetLeftMargin(.175)
    can.SetRightMargin(.2)
    can.SetRightMargin(.2)
    can.SetBottomMargin(.175)

    hist[channel].GetZaxis().SetTitleOffset(1.5);

    hist[channel].Draw('colz')

    can.SetLogz()

    can.Print(out_dir + '/' + channel.replace('#', '').replace(' = ', '').replace(' ', '_').replace('{', '').replace('}', '').replace('.', 'p') + '_jet_hist.png')

    can = ROOT.TCanvas(channel + '_jet_hist_lep', '', 1200, 1000)

    can.SetLeftMargin(.175)
    can.SetRightMargin(.2)
    can.SetRightMargin(.2)
    can.SetBottomMargin(.175)

    hist_lep[channel].GetZaxis().SetTitleOffset(1.5);

    hist_lep[channel].Divide(hist[channel])

    hist_lep[channel].Draw('colz')

    can.SetLogz()

    can.Print(out_dir + '/' + channel.replace('#', '').replace(' = ', '').replace(' ', '_').replace('{', '').replace('}', '').replace('.', 'p') + '_jet_hist_lep.png')

  can = ROOT.TCanvas('profile_x', '', 1200, 1000)

  can.SetLeftMargin(.175)
  can.SetBottomMargin(.175)

  leg = ROOT.TLegend(.6, .6, .9, .9)
  leg.SetFillStyle(0)

  for c, channel in enumerate(profile_x):

    profile_x[channel].SetMaximum(1.)
    profile_x[channel].SetMinimum(1e-6)
    
    profile_x[channel].SetLineColor(channels[channel][0])

    leg.AddEntry(profile_x[channel], channel, 'l')

    profile_x[channel].Draw('hist' if c == 0 else 'hist same')

  leg.Draw('same')

  can.SetLogy()

  can.Print(out_dir + '/jet_profile_x.png')

  can = ROOT.TCanvas('profile_x_lep', '', 1200, 1000)

  can.SetLeftMargin(.175)
  can.SetBottomMargin(.175)

  leg = ROOT.TLegend(.6, .6, .9, .9)
  leg.SetFillStyle(0)

  for c, channel in enumerate(profile_x_lep):

    profile_x_lep[channel].SetMaximum(1.)
    profile_x_lep[channel].SetMinimum(1e-6)
    
    profile_x_lep[channel].SetLineColor(channels[channel][0])

    leg.AddEntry(profile_x_lep[channel], channel, 'l')

    profile_x_lep[channel].Divide(profile_x[channel])

    profile_x_lep[channel].Draw('hist' if c == 0 else 'hist same')

  leg.Draw('same')

  can.SetLogy()

  can.Print(out_dir + '/jet_profile_x_lep.png')

  can = ROOT.TCanvas('profile_y', '', 1200, 1000)

  can.SetLeftMargin(.175)
  can.SetBottomMargin(.175)

  leg = ROOT.TLegend(.6, .6, .9, .9)
  leg.SetFillStyle(0)

  for c, channel in enumerate(profile_y):

    profile_y[channel].SetMaximum(1.)
    profile_y[channel].SetMinimum(1e-6)
    
    profile_y[channel].SetLineColor(channels[channel][0])

    leg.AddEntry(profile_y[channel], channel, 'l')

    profile_y[channel].Draw('hist' if c == 0 else 'hist same')

  leg.Draw('same')

  can.SetLogy()

  can.Print(out_dir + '/jet_profile_y.png')

  can = ROOT.TCanvas('profile_y_lep', '', 1200, 1000)

  can.SetLeftMargin(.175)
  can.SetBottomMargin(.175)

  leg = ROOT.TLegend(.6, .6, .9, .9)
  leg.SetFillStyle(0)

  for c, channel in enumerate(profile_y_lep):

    profile_y_lep[channel].SetMaximum(1.)
    profile_y_lep[channel].SetMinimum(1e-6)
    
    profile_y_lep[channel].SetLineColor(channels[channel][0])

    leg.AddEntry(profile_y_lep[channel], channel, 'l')

    profile_y_lep[channel].Divide(profile_y[channel])

    profile_y_lep[channel].Draw('hist' if c == 0 else 'hist same')

  leg.Draw('same')

  can.SetLogy()

  can.Print(out_dir + '/jet_profile_y_lep.png')

def calculate_secondary_vars(data):
  
  for channel in data:
   
    for p_type in ['ParticleFlowJet04', 'ParticleFlowJet08', 'Electron', 'Muon']:
  
      data[channel][p_type + '_pT_lead'] = [max(x) if len(x) > 0 else -99. for x in data[channel][p_type + '/' + p_type + '.PT']]
      data[channel][p_type + '_eta_lead'] = [sort_together([x, y])[1][0] if len(x) > 0 else -99. for x, y in zip(data[channel][p_type + '/' + p_type + '.PT'], data[channel][p_type + '/' + p_type + '.Eta'])]
      data[channel][p_type + '_N'] = [len(x) for x in data[channel][p_type + '/' + p_type + '.PT']]

    data[channel]['raw_e_n'] = []
    data[channel]['raw_mu_n'] = []
    data[channel]['raw_tau_n'] = []
    data[channel]['raw_e_lead_pT'] = []
    data[channel]['raw_e_lead_eta'] = []
    data[channel]['raw_mu_lead_pT'] = []
    data[channel]['raw_mu_lead_eta'] = []
    data[channel]['raw_tau_lead_pT'] = []
    data[channel]['raw_tau_lead_eta'] = []
    data[channel]['raw_e_lead_iso'] = []
    data[channel]['raw_e_avg_iso'] = []
    data[channel]['raw_mu_lead_iso'] = []
    data[channel]['raw_mu_avg_iso'] = []
    data[channel]['raw_tau_lead_iso'] = []
    data[channel]['raw_tau_avg_iso'] = []
    data[channel]['raw_tau_lead_nom_iso'] = []
    data[channel]['raw_tau_avg_nom_iso'] = []
    data[channel]['n_taus_in_lead_jet'] = []
    data[channel]['n_taus_in_sublead_jet'] = []
    data[channel]['n_taus_in_3rd_jet'] = []
    data[channel]['n_taus_in_4th_jet'] = []
    data[channel]['n_pi0s_in_lead_jet'] = []
    data[channel]['n_pi0s_in_sublead_jet'] = []
    data[channel]['n_pi0s_in_3rd_jet'] = []
    data[channel]['n_pi0s_in_4th_jet'] = []
    data[channel]['n_pipms_in_lead_jet'] = []
    data[channel]['n_pipms_in_sublead_jet'] = []
    data[channel]['n_pipms_in_3rd_jet'] = []
    data[channel]['n_pipms_in_4th_jet'] = []
    data[channel]['n_photons_in_lead_jet'] = []
    data[channel]['n_photons_in_sublead_jet'] = []
    data[channel]['n_photons_in_3rd_jet'] = []
    data[channel]['n_photons_in_4th_jet'] = []
    data[channel]['pt_frac_from_photons_in_lead_jet'] = []
    data[channel]['pt_frac_from_pi0s_in_lead_jet'] = []
    data[channel]['pt_frac_from_pipms_in_lead_jet'] = []
    data[channel]['pt_frac_from_taus_in_lead_jet'] = []
    data[channel]['pt_frac_from_es_in_lead_jet'] = []
    data[channel]['pt_frac_from_mus_in_lead_jet'] = []
    data[channel]['n_es_in_lead_jet'] = []
    data[channel]['n_es_in_sublead_jet'] = []
    data[channel]['n_es_in_3rd_jet'] = []
    data[channel]['n_es_in_4th_jet'] = []
    data[channel]['n_mus_in_lead_jet'] = []
    data[channel]['n_mus_in_sublead_jet'] = []
    data[channel]['n_mus_in_3rd_jet'] = []
    data[channel]['n_mus_in_4th_jet'] = []
    data[channel]['mjj_ak4'] = []
    data[channel]['mjjT_ak4'] = []
    data[channel]['mjj_ak8'] = []
    data[channel]['mjjT_ak8'] = []
    data[channel]['deltaPhi_met_ak4_lead'] = []
    data[channel]['deltaPhi_met_ak8_lead'] = []
    data[channel]['deltaPhi_met_ak4_sublead'] = []
    data[channel]['deltaPhi_met_ak8_sublead'] = []

    for pid_arr_t, pt_arr_t, eta_arr_t, phi_arr_t, pt_arr_j, eta_arr_j, phi_arr_j, pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p, pt_arr_y, eta_arr_y, phi_arr_y, p4_arr_j8, p4_arr_j4, pt_met, phi_met in zip(data[channel]['EFlowTrack/EFlowTrack.PID'], data[channel]['EFlowTrack/EFlowTrack.PT'], data[channel]['EFlowTrack/EFlowTrack.Eta'], data[channel]['EFlowTrack/EFlowTrack.Phi'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.PT'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.Eta'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.Eta'], data[channel]['Particle/Particle.PT'], data[channel]['Particle/Particle.Eta'], data[channel]['Particle/Particle.Phi'], data[channel]['Particle/Particle.PID'], data[channel]['Particle/Particle.Status'], data[channel]['Photon/Photon.PT'], data[channel]['Photon/Photon.Eta'], data[channel]['Photon/Photon.Phi'], data[channel]['<DelphesP4>ParticleFlowJet08'], data[channel]['<DelphesP4>ParticleFlowJet04'], data[channel]['MissingET/MissingET.MET'], data[channel]['MissingET/MissingET.Phi']):

      eflows    = [(pt, eta, phi) for pid, pt, eta, phi in zip(pid_arr_t, pt_arr_t, eta_arr_t, phi_arr_t)]
      electrons = [(pt, eta, phi) for pid, pt, eta, phi in zip(pid_arr_t, pt_arr_t, eta_arr_t, phi_arr_t) if abs(pid) == 11]
      muons     = [(pt, eta, phi) for pid, pt, eta, phi in zip(pid_arr_t, pt_arr_t, eta_arr_t, phi_arr_t) if abs(pid) == 13]
      taus      = [(pt, eta, phi) for pt, eta, phi, pid, status in zip(pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p) if abs(pid) == 15 and status == 91]
      jets      = [(pt, eta, phi) for pt, eta, phi in zip(pt_arr_j, eta_arr_j, phi_arr_j)]
      photons   = [(pt, eta, phi, status) for pt, eta, phi, pid, status in zip(pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p) if abs(pid) == 22]
      #photons  = [(pt, eta, phi) for pt, eta, phi in zip(pt_arr_y, eta_arr_y, phi_arr_y)]
      pizeros   = [(pt, eta, phi, status) for pt, eta, phi, pid, status in zip(pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p) if abs(pid) == 111]
      pipms     = [(pt, eta, phi, status) for pt, eta, phi, pid, status in zip(pt_arr_p, eta_arr_p, phi_arr_p, pid_arr_p, status_arr_p) if abs(pid) == 211]

      eflows    = sorted(eflows, key=lambda x: x[0], reverse=True)
      electrons = sorted(electrons, key=lambda x: x[0], reverse=True)
      muons     = sorted(muons, key=lambda x: x[0], reverse=True)
      taus      = sorted(taus, key=lambda x: x[0], reverse=True)
      jets      = sorted(jets, key=lambda x: x[0], reverse=True)
      photons   = sorted(photons, key=lambda x: x[0], reverse=True)
      pizeros   = sorted(pizeros, key=lambda x: x[0], reverse=True)
      pipms     = sorted(pipms, key=lambda x: x[0], reverse=True)
      p4_arr_j4 = sorted(p4_arr_j4, key=lambda x: x.Pt(), reverse=True)
      p4_arr_j8 = sorted(p4_arr_j8, key=lambda x: x.Pt(), reverse=True)

      data[channel]['raw_e_n'].append(len(electrons))
      data[channel]['raw_mu_n'].append(len(muons))
      data[channel]['raw_tau_n'].append(len(taus))

      data[channel]['raw_e_lead_pT'].append(electrons[0][0] if len(electrons)>0 else -1.)
      data[channel]['raw_e_lead_eta'].append(electrons[0][1] if len(electrons)>0 else -99.)
      data[channel]['raw_mu_lead_pT'].append(muons[0][0] if len(muons)>0 else -1.)
      data[channel]['raw_mu_lead_eta'].append(muons[0][1] if len(muons)>0 else -99.)
      data[channel]['raw_tau_lead_pT'].append(taus[0][0] if len(taus)>0 else -1.)
      data[channel]['raw_tau_lead_eta'].append(taus[0][1] if len(taus)>0 else -99.)

      leptons = electrons + muons
      
      # lepton inter-isolation of electrons
      for e in range(len(electrons)):

        iso_e  = 0.

        pt_e = electrons[e][0]
        eta_e = electrons[e][1]
        phi_e = electrons[e][2]
        
        for l in range(len(leptons)):
          
          if e == l: continue

          pt_l = leptons[l][0]
          eta_l = leptons[l][1]
          phi_l = leptons[l][2]
          
          delta_R = math.sqrt((eta_e - eta_l)**2 + (phi_e - phi_l)**2)

          if delta_R <= 0.5: iso_e += pt_l / pt_e if pt_e != 0. else 0.

        electrons[e] = (*(electrons[e]), iso_e) # add iso as element 3 of electrons tuple

      data[channel]['raw_e_lead_iso'].append(electrons[0][3] if len(electrons)>0 else -1.)
      data[channel]['raw_e_avg_iso'].append(np.mean([e[3] for e in electrons]) if len(electrons)>0 else -1.)

      # lepton inter-isolation of muons
      for m in range(len(muons)):

        iso_mu  = 0.

        pt_mu = muons[m][0]
        eta_mu = muons[m][1]
        phi_mu = muons[m][2]
        
        for l in range(len(leptons)):
          
          if m + len(electrons) == l: continue

          pt_l = leptons[l][0]
          eta_l = leptons[l][1]
          phi_l = leptons[l][2]
          
          delta_R = math.sqrt((eta_mu - eta_l)**2 + (phi_mu - phi_l)**2)

          if delta_R <= 0.5: iso_mu += pt_l / pt_mu if pt_mu != 0. else 0.

        muons[m] = (*(muons[m]), iso_mu) # add iso as element 3 of muons tuple

      data[channel]['raw_mu_lead_iso'].append(muons[0][3] if len(muons)>0 else -1.)
      data[channel]['raw_mu_avg_iso'].append(np.mean([m[3] for m in muons]) if len(muons)>0 else -1.)

      # lepton inter-isolation of taus
      for t in range(len(taus)):

        iso_tau  = 0.

        pt_tau = taus[t][0]
        eta_tau = taus[t][1]
        phi_tau = taus[t][2]
        
        for l in range(len(leptons)):
          
          pt_lep = leptons[l][0]
          eta_lep = leptons[l][1]
          phi_lep = leptons[l][2]
          
          delta_R = math.sqrt((eta_tau - eta_lep)**2 + (phi_tau - phi_lep)**2)

          if delta_R <= 0.5: iso_tau += pt_lep / pt_tau if pt_tau != 0. else 0.

        taus[t] = (*(taus[t]), iso_tau) # add iso as element 3 of taus tuple

      data[channel]['raw_tau_lead_iso'].append(taus[0][3] if len(taus)>0 else -1.)
      data[channel]['raw_tau_avg_iso'].append(np.mean([t[3] for t in taus]) if len(taus)>0 else -1.)

      # nominal inter-isolation of taus
      for t in range(len(taus)):

        iso_tau  = 0.

        pt_tau = taus[t][0]
        eta_tau = taus[t][1]
        phi_tau = taus[t][2]
        
        for l in range(len(eflows)):
          
          pt_lep = eflows[l][0]
          eta_lep = eflows[l][1]
          phi_lep = eflows[l][2]
          
          delta_R = math.sqrt((eta_tau - eta_lep)**2 + (phi_tau - phi_lep)**2)

          if delta_R <= 0.5: iso_tau += pt_lep / pt_tau if pt_tau != 0. else 0.

        taus[t] = (*(taus[t]), iso_tau) # add iso as element 4 of taus tuple

      data[channel]['raw_tau_lead_nom_iso'].append(taus[0][4] if len(taus)>0 else -1.)
      data[channel]['raw_tau_avg_nom_iso'].append(np.mean([t[4] for t in taus]) if len(taus)>0 else -1.)

      # taus and pions in jets
      for j in range(len(jets)):

        n_taus = 0
        n_mus = 0
        n_es = 0
        n_pizeros = 0
        n_pipms = 0
        n_photons = 0
        pt_frac_from_photons = 0.
        pt_frac_from_pi0s = 0.
        pt_frac_from_pipms = 0.
        pt_frac_from_taus = 0.
        pt_frac_from_mus = 0.
        pt_frac_from_es = 0.
        
        pt_jet = jets[j][0]
        eta_jet = jets[j][1]
        phi_jet = jets[j][2]

        for e in range(len(electrons)):

          pt_e = electrons[e][0]
          eta_e = electrons[e][1]
          phi_e = electrons[e][2]
          
          delta_R = math.sqrt((eta_jet - eta_e)**2 + (phi_jet - phi_e)**2)

          if delta_R <= 0.8:

            n_es += 1

            pt_frac_from_es += pt_e

        for m in range(len(muons)):

          pt_m = muons[m][0]
          eta_m = muons[m][1]
          phi_m = muons[m][2]
          
          delta_R = math.sqrt((eta_jet - eta_m)**2 + (phi_jet - phi_m)**2)

          if delta_R <= 0.8:

            n_mus += 1

            pt_frac_from_mus += pt_m

        for t in range(len(taus)):

          pt_tau = taus[t][0]
          eta_tau = taus[t][1]
          phi_tau = taus[t][2]
          
          delta_R = math.sqrt((eta_jet - eta_tau)**2 + (phi_jet - phi_tau)**2)

          if delta_R <= 0.8:

            n_taus += 1

            pt_frac_from_taus += pt_tau

        jets[j] = (*(jets[j]), n_taus) # add n_taus as element 3 of jets tuple

        for p in range(len(pizeros)):

          pt_pi = pizeros[p][0]
          eta_pi = pizeros[p][1]
          phi_pi = pizeros[p][2]
          
          delta_R = math.sqrt((eta_jet - eta_pi)**2 + (phi_jet - phi_pi)**2)

          if delta_R <= 0.8:

            n_pizeros += 1

            pt_frac_from_pi0s += pt_pi

        jets[j] = (*(jets[j]), n_pizeros) # add n_pizeros as element 4 of jets tuple

        for p in range(len(pipms)):

          pt_pi = pipms[p][0]
          eta_pi = pipms[p][1]
          phi_pi = pipms[p][2]
          
          delta_R = math.sqrt((eta_jet - eta_pi)**2 + (phi_jet - phi_pi)**2)

          if delta_R <= 0.8:

            n_pipms += 1

            pt_frac_from_pipms += pt_pi

        jets[j] = (*(jets[j]), n_pipms) # add n_pipms as element 5 of jets tuple

        for y in range(len(photons)):

          pt_y = photons[y][0]
          eta_y = photons[y][1]
          phi_y = photons[y][2]
          
          delta_R = math.sqrt((eta_jet - eta_y)**2 + (phi_jet - phi_y)**2)

          if delta_R <= 0.8:

            n_photons += 1

            pt_frac_from_photons += pt_y

        pt_frac_from_photons /= pt_jet
        pt_frac_from_pi0s /= pt_jet
        pt_frac_from_pipms /= pt_jet
        pt_frac_from_taus /= pt_jet
        pt_frac_from_mus /= pt_jet
        pt_frac_from_es /= pt_jet

        jets[j] = (*(jets[j]), n_photons) # add n_photons as element 6 of jets tuple
    
        jets[j] = (*(jets[j]), pt_frac_from_photons) # add pt_frac_from_photons as element 7 of jets tuple
        jets[j] = (*(jets[j]), pt_frac_from_pi0s) # add pt_frac_from_pi0s as element 8 of jets tuple
        jets[j] = (*(jets[j]), pt_frac_from_pipms) # add pt_frac_from_pipms as element 9 of jets tuple
        jets[j] = (*(jets[j]), pt_frac_from_taus) # add pt_frac_from_taus as element 10 of jets tuple
        jets[j] = (*(jets[j]), pt_frac_from_mus) # add pt_frac_from_mus as element 11 of jets tuple
        jets[j] = (*(jets[j]), pt_frac_from_es) # add pt_frac_from_es as element 12 of jets tuple

        jets[j] = (*(jets[j]), n_es) # add n_es as element 13 of jets tuple
        jets[j] = (*(jets[j]), n_mus) # add n_mus as element 14 of jets tuple


      data[channel]['n_taus_in_lead_jet'].append(jets[0][3] if len(jets)>0 else -1.)
      data[channel]['n_taus_in_sublead_jet'].append(jets[1][3] if len(jets)>1 else -1.)
      data[channel]['n_taus_in_3rd_jet'].append(jets[2][3] if len(jets)>2 else -1.)
      data[channel]['n_taus_in_4th_jet'].append(jets[3][3] if len(jets)>3 else -1.)

      data[channel]['n_pi0s_in_lead_jet'].append(jets[0][4] if len(jets)>0 else -1.)
      data[channel]['n_pi0s_in_sublead_jet'].append(jets[1][4] if len(jets)>1 else -1.)
      data[channel]['n_pi0s_in_3rd_jet'].append(jets[2][4] if len(jets)>2 else -1.)
      data[channel]['n_pi0s_in_4th_jet'].append(jets[3][4] if len(jets)>3 else -1.)

      data[channel]['n_pipms_in_lead_jet'].append(jets[0][5] if len(jets)>0 else -1.)
      data[channel]['n_pipms_in_sublead_jet'].append(jets[1][5] if len(jets)>1 else -1.)
      data[channel]['n_pipms_in_3rd_jet'].append(jets[2][5] if len(jets)>2 else -1.)
      data[channel]['n_pipms_in_4th_jet'].append(jets[3][5] if len(jets)>3 else -1.)
      
      data[channel]['n_photons_in_lead_jet'].append(jets[0][6] if len(jets)>0 else -1.)
      data[channel]['n_photons_in_sublead_jet'].append(jets[1][6] if len(jets)>1 else -1.)
      data[channel]['n_photons_in_3rd_jet'].append(jets[2][6] if len(jets)>2 else -1.)
      data[channel]['n_photons_in_4th_jet'].append(jets[3][6] if len(jets)>3 else -1.)

      data[channel]['pt_frac_from_photons_in_lead_jet'].append(jets[0][7] if len(jets)>0 else -1.)
      data[channel]['pt_frac_from_pi0s_in_lead_jet'].append(jets[0][8] if len(jets)>0 else -1.)
      data[channel]['pt_frac_from_pipms_in_lead_jet'].append(jets[0][9] if len(jets)>0 else -1.)
      data[channel]['pt_frac_from_taus_in_lead_jet'].append(jets[0][10] if len(jets)>0 else -1.)
      data[channel]['pt_frac_from_mus_in_lead_jet'].append(jets[0][11] if len(jets)>0 else -1.)
      data[channel]['pt_frac_from_es_in_lead_jet'].append(jets[0][12] if len(jets)>0 else -1.)

      data[channel]['n_es_in_lead_jet'].append(jets[0][13] if len(jets)>0 else -1.)
      data[channel]['n_es_in_sublead_jet'].append(jets[1][13] if len(jets)>1 else -1.)
      data[channel]['n_es_in_3rd_jet'].append(jets[2][13] if len(jets)>2 else -1.)
      data[channel]['n_es_in_4th_jet'].append(jets[3][13] if len(jets)>3 else -1.)

      data[channel]['n_mus_in_lead_jet'].append(jets[0][14] if len(jets)>0 else -1.)
      data[channel]['n_mus_in_sublead_jet'].append(jets[1][14] if len(jets)>1 else -1.)
      data[channel]['n_mus_in_3rd_jet'].append(jets[2][14] if len(jets)>2 else -1.)
      data[channel]['n_mus_in_4th_jet'].append(jets[3][14] if len(jets)>3 else -1.)

      # deltaPhi met jets
      if len(p4_arr_j4) > 0: data[channel]['deltaPhi_met_ak4_lead'].append(phi_subtract(phi_met, p4_arr_j4[0].Phi()))
      else: data[channel]['deltaPhi_met_ak4_lead'].append(-99.)
      if len(p4_arr_j4) > 1: data[channel]['deltaPhi_met_ak4_sublead'].append(phi_subtract(phi_met, p4_arr_j4[1].Phi()))
      else: data[channel]['deltaPhi_met_ak4_sublead'].append(-99.)

      if len(p4_arr_j8) > 0: data[channel]['deltaPhi_met_ak8_lead'].append(phi_subtract(phi_met, p4_arr_j8[0].Phi()))
      else: data[channel]['deltaPhi_met_ak8_lead'].append(-99.)
      if len(p4_arr_j8) > 1: data[channel]['deltaPhi_met_ak8_sublead'].append(phi_subtract(phi_met, p4_arr_j8[1].Phi()))
      else: data[channel]['deltaPhi_met_ak8_sublead'].append(-99.)

      # dijet mass
      if len(p4_arr_j4) > 2:

        jj4 = p4_arr_j4[0] + p4_arr_j4[1]

        data[channel]['mjj_ak4'].append(jj4.M())
        data[channel]['mjjT_ak4'].append(jj4.Mt())

      else:

        data[channel]['mjj_ak4'].append(-99.)
        data[channel]['mjjT_ak4'].append(-99.)

      if len(p4_arr_j8) > 2:

        jj8 = p4_arr_j8[0] + p4_arr_j8[1]

        data[channel]['mjj_ak8'].append(jj8.M())
        data[channel]['mjjT_ak8'].append(jj8.Mt())

      else:

        data[channel]['mjj_ak8'].append(-99.)
        data[channel]['mjjT_ak8'].append(-99.)
        
  return data

def event_selection(data):
  
  print('applying event selection...')
  
  for channel in data:
    data[channel]['event_selection'] = []

    # at least 2 AK8 Jets with delta Eta < 1.5 and minimum pt > 200 GeV
    for pt, eta in zip(data[channel]['ParticleFlowJet08/ParticleFlowJet08.PT'], data[channel]['ParticleFlowJet08/ParticleFlowJet08.Eta']):

      if len(pt) < 2:
        data[channel]['event_selection'].append(False)
        continue

      event_selection = False

      for i in range(0, len(pt)):
        for j in range(0, len(pt)):
          if i == j: continue
          if pt[i] < 200. : continue
          delta_eta = abs(eta[i] - eta[j])
          if delta_eta < 1.5: event_selection = True

      data[channel]['event_selection'].append(event_selection)

  data_selected = {}

  for channel in data:
    data_selected[channel] = {}
    for var_name in data[channel]:
      data_selected[channel][var_name] = []
      for i, selection in enumerate(data[channel]['event_selection']):
        if selection: data_selected[channel][var_name].append(data[channel][var_name][i])

  return data_selected


def load_data(n_entries=-1):
 
  if n_entries > 0: print('loading', n_entries, 'events per sample')
 
  data = {}

  non_delphes_vars = [var for var in primary_vars if not '<DelphesP4>' in var]
  delphes_vars = [var for var in primary_vars if '<DelphesP4>' in var]
 
  for channel in channels:
   
    data[channel] = {}
    
    trees = []

    for in_file_name in channels[channel][1]:
      
      tree_name = in_file_name + ':' + in_tree_name
      
      print('loading:', tree_name)

      # load non-delphes variables
      arr = uproot.open(tree_name, filter_names=non_delphes_vars).arrays(non_delphes_vars, library='np')

      # load delphes variables
      for var in delphes_vars:
        
        var_vec = []

        branch_name = var.replace('<DelphesP4>', '')

        in_file = ROOT.TFile.Open(in_file_name)

        in_tree = in_file.Get(in_tree_name)
        tree_reader = ROOT.ExRootTreeReader(in_tree)
        
        branch = tree_reader.UseBranch(branch_name)
        
        for entry in range(tree_reader.GetEntries()):
        
          tree_reader.ReadEntry(entry)

          var_vec.append(np.array([branch.At(i).P4() for i in range(branch.GetEntries())]))

        arr[var] = var_vec
        
      if n_entries > 0:
       
        for key in arr: arr[key] = arr[key][:n_entries]

      trees.append(arr)

    for var in primary_vars: data[channel][var] = np.concatenate([tree[var] for tree in trees])

  return data


def plot_var(data, var_name):
 
  var_options = plotting_vars[var_name]

  hist = {}

  if ' ' in var_name: # 2d plot
    
    var_name_x, var_name_y = var_name.split(' ')

    for k, channel in enumerate(data):

      can = ROOT.TCanvas(var_name + channel, '', 1200, 1000)

      can.SetLeftMargin(.175)
      can.SetRightMargin(.2)
      can.SetRightMargin(.2)
      can.SetBottomMargin(.175)
    
      hist[channel] = ROOT.TH2F(channel + var_name, ';' + var_options[0][0] + ';' + var_options[0][1] + ';events (normalised)', *var_options[1])

      for entry_x, entry_y in zip(data[channel][var_name_x], data[channel][var_name_y]): hist[channel].Fill(entry_x, entry_y)

      # add overflow and underflow along the y axis
      for y in range(1, hist[channel].GetNbinsY() + 1):

        hist[channel].SetBinContent(hist[channel].GetNbinsX(), y, hist[channel].GetBinContent(hist[channel].GetNbinsX(), y) + hist[channel].GetBinContent(hist[channel].GetNbinsX() + 1, y)) # add overflow to last bin
        hist[channel].SetBinContent(1, y, hist[channel].GetBinContent(1, y) + hist[channel].GetBinContent(0, y)) # add underflow to first bin

      # add overflow and underflow along the x axis
      for x in range(1, hist[channel].GetNbinsX() + 1):

        hist[channel].SetBinContent(x, hist[channel].GetNbinsY(), hist[channel].GetBinContent(x, hist[channel].GetNbinsY()) + hist[channel].GetBinContent(x, hist[channel].GetNbinsY() + 1)) # add overflow to last bin
        hist[channel].SetBinContent(x, 1, hist[channel].GetBinContent(x, 1) + hist[channel].GetBinContent(x, 0)) # add underflow to first bin
    
      hist[channel].Scale(1./hist[channel].Integral() if hist[channel].Integral() > 0. else 1.)

      hist[channel].GetZaxis().SetTitleOffset(1.5);

      hist[channel].Draw('colz')

      can.SetLogz()

      can.Print(out_dir + '/2d_' + channel.replace('#', '').replace(' = ', '').replace(' ', '_').replace('{', '').replace('}', '').replace('.', 'p') + '_' + var_name.replace('/', '_').replace('.', '_').replace(' ', '_vs_') + '.png')

  else: # 1d plot

    can = ROOT.TCanvas(var_name, '', 1200, 1000)

    can.SetLeftMargin(.175)
    can.SetBottomMargin(.175)

    leg = ROOT.TLegend(.6, .6, .9, .9)
    leg.SetFillStyle(0)

    for k, channel in enumerate(data):
    
      hist[channel] = ROOT.TH1F(channel + var_name, ';' + var_options[0] + ';events (normalised)', *var_options[1])
      hist[channel].SetLineColor(channels[channel][0])

      for entry in data[channel][var_name]: hist[channel].Fill(entry)

      hist[channel].SetBinContent(hist[channel].GetNbinsX(), hist[channel].GetBinContent(hist[channel].GetNbinsX()) + hist[channel].GetBinContent(hist[channel].GetNbinsX() + 1)) # add overflow to last bin
      hist[channel].SetBinContent(1, hist[channel].GetBinContent(1) + hist[channel].GetBinContent(0)) # add underflow to first bin

      hist[channel].Scale(1./hist[channel].Integral() if hist[channel].Integral() > 0. else 1.)

      hist[channel].Draw('hist' if k==0 else 'hist same')

      leg.AddEntry(hist[channel], channel, 'l')

    y_max = -99.

    for channel in hist:
      if hist[channel].GetMaximum() > y_max: y_max = hist[channel].GetMaximum()

    for channel in hist:
      hist[channel].SetMinimum(10. ** (-6))
      hist[channel].SetMaximum(y_max * 10.)
      hist[channel].SetMaximum(85.)

    leg.Draw('same')

    can.SetLogy()

    can.Print(out_dir + '/' + var_name.replace('/', '_').replace('.', '_') + '.png')


if __name__ == '__main__':
 
 data = load_data(n_entries=-1)
 
 if jet_image_mode:
   
   jet_images = get_jet_images(data)


   jet_images = translate_jet_images(jet_images, by='jetaxis')

   for channel in channels: jet_images[channel] = [calc_mean_and_pca(img) for img in jet_images[channel]]
   
   # standard preprocessing
#   jet_images = translate_jet_images(jet_images, by='mean')
#   jet_images = rotate_jet_images(jet_images, by='pca0')
#   jet_images = reflect_jet_images(jet_images)

   # custom preprocessing
#   jet_images = translate_jet_images(jet_images, by='mean')
   jet_images = rotate_jet_images(jet_images, by='met')
   jet_images = reflect_jet_images(jet_images)

   plot_jet_images(jet_images, 50)
   plot_jet_hists(jet_images)

 else:

   data = calculate_secondary_vars(data)
#   data = event_selection(data)

   for var_name in plotting_vars: plot_var(data, var_name)
