* based on: https://twiki.cern.ch/twiki/bin/view/CMSPublic/MadgraphTutorial

* For HT/pT thresholds see end of chap 5 in: https://cds.cern.ch/record/2778946/files/EXO-19-020-pas.pdf

# install

```
wget https://launchpad.net/mg5amcnlo/3.0/3.4.x/+download/MG5_aMC_v2.9.11.tar.gz
tar -xvzf MG5_aMC_v2.9.11.tar.gz
rm $_
python3.8 -m venv env
. env/bin/activate
pip install six
cd MG5_aMC_v2_9_11
vim input/.autoupdate
# change first line to: `version_nb   306`
apt intall gfortran fort77
python bin/mg5_aMC
> install pythia8
> y
> install pythia-pgs
> y
> quit
cd ..
wget http://cp3.irmp.ucl.ac.be/downloads/Delphes-3.5.0.tar.gz
tar -xvzf Delphes-3.5.0.tar.gz
rm $_
cd Delphes-3.5.0/
make
cd ..
# upload the `UFO_and_FR.tar.gz` UFO file from Hugues
tar -xvzf UFO_and_FR.tar.gz
rm $_
cp -r UFO_and_FR/Model_II_UFO MG5_aMC_v2_9_11/models/
cd MG5_aMC_v2_9_11
vim input/mg5_configuration.txt
# change line 144 to `delphes_path = ../Delphes-3.5.0`
cd ..
git clone git@github.com:cesarecazzaniga/SVJ_leptons_WG5_models.git
```

# Samples from Tier3

```
voms-proxy-init -voms atlas
gfal-ls --verbose srm://t3se01.psi.ch:8443/srm/managerv2?SFN=/pnfs/psi.ch/cms/trivcat/store/t3groups/ethz-susy/darkshowers/samples/backgrounds/
```

result:

```
ZJets
TTbar
QCD_dijet
WJets
small_bkgs_onlyPY8
small_bkgs_MG5PY8
QCD_dijet_lowPT
```

To get the file names that are used in `cut_based_analysis_ntuplemaker.C`:

```
for channel in "ZJets" "WJets" "TTbar" "QCD_dijet" "QCD_dijet_lowPT"; do
  gfal-ls --verbose srm://t3se01.psi.ch:8443/srm/managerv2?SFN=/pnfs/psi.ch/cms/trivcat/store/t3groups/ethz-susy/darkshowers/samples/backgrounds/${channel} > samples_${channel}.txt
done
```

* Note: in `cut_based_analysis_ntuplemaker.C` itself the redirector `root://t3se01.psi.ch:1094//store/t3groups/ethz-susy/darkshowers/samples/backgrounds/` is used instead

Then make the nTuples (with condor):

```
mkdir logs
export X509_USER_PROXY=${HOME}/private/.x509up_${UID}
voms-proxy-init -voms atlas -rfc -out ${HOME}/private/.x509up_${UID} -valid 192:00
condor_submit condor.sub 
```

once done:

```
for channel in "ZJets" "WJets" "TTbar" "QCD_dijet" "QCD_dijet_lowPT"; do
  hadd ${channel}.root ~/eos/SVJnTuples/${channel}_[0-9]*
done
```

# Small local samples

* downdoad from: https://cernbox.cern.ch/index.php/s/ButyUF1tGPh6ifF
* or better from eos, like: `/eos/user/c/cazzanig/samples_svjl_study_wg5/model_2/svjtaus_onlypy8`
* create file structure like this:

```
samples_from_cesare/
├── Backgrounds
│   ├── QCD_c_flavour_ptHat_200GeV.root
│   ├── QCD_gluon_ptHat_200GeV.root
│   ├── QCD_heavy_flavour_ptHat_200GeV.root
│   └── QCD_light_flavour_ptHat_200GeV.root
├── model_1
│   └── t_channel_pair_production
│       └── HL_LHC_reco
│           ├── t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_3_sqrts_13TeV.root
│           ├── t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_5_sqrts_13TeV.root
│           ├── t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_7_sqrts_13TeV.root
│           └── t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_sqrts_13TeV.root
└── model_2
```

# run
```
. env/bin/activate
cd MG5_aMC_v2_9_11
python bin/mg5_aMC
> set nb_core 1
> convert model ../SVJ_leptons_WG5_models/models/Category_1_and_2_nf_UFO
> import model ../SVJ_leptons_WG5_models/models/Category_1_and_2_nf_UFO
> generate p p  > nf nf~
> output ../output
> launch ../output
> pythia=ON
> delphes=ON
...
```

...or just use the `MG5_card` in the base dir which is based on Cesare's card in `SVJ_leptons_WG5_models/MG5_launchers/mymg5_t_channel_XD2TeV_XE400GeV.txt`:

```
. env/bin/activate
cd MG5_aMC_v2_9_11
python bin/mg5_aMC -f ../MG5_card.txt
```
the output root file will be in:

```
output/Events/run_01/tag_1_delphes_events.root
```
