import ROOT

ROOT.gInterpreter.Declare('R__ADD_INCLUDE_PATH(Delphes-3.5.0);')
ROOT.gInterpreter.Declare('R__ADD_INCLUDE_PATH(Delphes-3.5.0/external);')
ROOT.gInterpreter.Declare('R__LOAD_LIBRARY(Delphes-3.5.0/libDelphes);')
ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')

inFile = ROOT.TFile.Open('samples_from_cesare/model_1/t_channel_pair_production/HL_LHC_reco/t_channel_XD2TeV_XE400GeV_LambdaD_1_LambdaE_0_2_rinv_0_7_sqrts_13TeV.root')
tree = inFile.Get('Delphes')
treeReader = ROOT.ExRootTreeReader(tree)

branchJet = treeReader.UseBranch('ParticleFlowJet08')

nEntries = treeReader.GetEntries()

for entry in range(nEntries):

  treeReader.ReadEntry(entry)

  nJets = branchJet.GetEntries()

  print(nJets)
