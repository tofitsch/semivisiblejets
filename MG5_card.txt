set nb_core 1
convert model ../SVJ_leptons_WG5_models/models/Category_1_and_2_nf_UFO
import model ../SVJ_leptons_WG5_models/models/Category_1_and_2_nf_UFO
generate p p  > nf nf~
output ../output
launch ../output
pythia=ON
delphes=ON
set nevents 100
set ebeam1 6500
set ebeam2 6500
set MXSuL 2000
set MXSdL 2000
set MXSuR 2000
set MXSdR 2000
set MXSeL 400
set MXSnu 400
set MXSeR 400
set Mnf 10.2
set lambdaXSl1 0.0  
set lambdaXSl2 0.0
set lambdaXSl3 0.2
set lambdaXSeR1 0.0
set lambdaXSeR2 0.0
set lambdaXSeR3 0.2
set lambdaXSq1  1.0 
set lambdaXSq2  0.0
set lambdaXSq3  0.0
set lambdaXSuR1 0.0
set lambdaXSuR2 1.0
set lambdaXSuR3 0.0
set lambdaXSdR1 0.0
set lambdaXSdR2 0.0
set lambdaXSdR3 0.0
exit
