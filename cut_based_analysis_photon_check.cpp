// event selection based on: https://doi.org/10.1007/JHEP06(2022)156

#ifdef __CLING__
R__ADD_INCLUDE_PATH(Delphes-3.5.0);
R__ADD_INCLUDE_PATH(Delphes-3.5.0/external);
R__LOAD_LIBRARY(Delphes-3.5.0/libDelphes);
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#else
class ExRootTreeReader;
class ExRootResult;
#endif

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

const bool debug = true; //only do a few events and only one sample
const int debugEvents = 1e3; //only used in debug mode

const float NAN_value = -9999.;

void cut_based_analysis_photon_check(){
  
  //-Options
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);

  //-Read files into chain
  vector<string> inFileNames = {"samples_from_cesare/model_2/svjtaus_onlypy8/alpha_0_5_r_inv_0_3_MZp_3000GeV.root"};
  //vector<string> inFileNames = {"samples_from_cesare/Backgrounds/reco_pythia8_CMS/gluons_QCD.root"};
  //vector<string> inFileNames = {"samples_from_cesare/Backgrounds/reco_pythia8_CMS/TTbar.root"};

  TChain chain("Delphes");
  for(string inFileName: inFileNames) chain.Add(inFileName.c_str());

  ExRootTreeReader *treeReader = new ExRootTreeReader(&chain);

  //-Define branches to read from chain
  TClonesArray *inTreeBranchEvent = treeReader->UseBranch("Event");
  TClonesArray *inTreeBranchGenParticle = treeReader->UseBranch("Particle");
  TClonesArray *inTreeBranchEFlowTrack = treeReader->UseBranch("EFlowTrack");
  TClonesArray *inTreeBranchElectron = treeReader->UseBranch("Electron");
  TClonesArray *inTreeBranchMuon = treeReader->UseBranch("Muon");
  TClonesArray *inTreeBranchAK8Jet = treeReader->UseBranch("FatJet");

  //-Define output histograms for inter-isolation study
  TH1* hist_e = new TH1F("hist_e", "#gamma from e;p_{T}(#gamma) [GeV];entries / 10 GeV", 50, 0., 500.);
  TH1* hist_mu = new TH1F("hist_mu", "#gamma from #mu;p_{T}(#gamma) [GeV];entries / 10 GeV", 50, 0., 500.);
  TH1* hist_tau = new TH1F("hist_tau", "#gamma from #tau;p_{T}(#gamma) [GeV];entries / 10 GeV", 50, 0., 500.);

  TH1* hist_iso_e = new TH1F("hist_iso_e", "interisolation of e;I_{int}(e);entries", 20, 0., 10.);
  TH1* hist_iso_mu = new TH1F("hist_iso_mu", "interisolation of #mu;I_{int}(#mu);entries", 20, 0., 10.);

  TH1* hist_iso_e_with_gamma = new TH1F("hist_iso_e_with_gamma", "interisolation of e;I_{int}(e);entries", 20, 0., 10.);
  TH1* hist_iso_mu_with_gamma = new TH1F("hist_iso_mu_with_gamma", "interisolation of #mu;I_{int}(#mu);entries", 20, 0., 10.);

  //-Loop over events
  const int nEntries = debug ? debugEvents : treeReader->GetEntries();
  cout<<nEntries<<" entries"<<endl;

  for(Int_t entry = 0; entry < nEntries; ++entry){

    treeReader->ReadEntry(entry);

    //-Inter-Isolation
    //---Find photons, fill hists
    vector<TLorentzVector> pf_electrons;
    vector<TLorentzVector> pf_muons;
    vector<TLorentzVector> pf_leptons;
    vector<TLorentzVector> photons;

    //---Electrons
    for(int i=0; i<inTreeBranchEFlowTrack->GetEntries(); i++){

      Track *electron = (Track*) inTreeBranchEFlowTrack->At(i);

      if(abs(electron->PID) != 11) continue; // pid 11 -> electron

      if(electron->PT < 10. || electron->Eta > 2.4) continue;

      pf_electrons.push_back(electron->P4());
      pf_leptons.push_back(electron->P4());

    }

    //---Muons
    for(int i=0; i<inTreeBranchEFlowTrack->GetEntries(); i++){

      Track *muon = (Track*) inTreeBranchEFlowTrack->At(i);

      if(abs(muon->PID) != 13) continue; // pid 11 -> muon

      if(muon->PT < 10. || muon->Eta > 2.4) continue;

      pf_muons.push_back(muon->P4());
      pf_leptons.push_back(muon->P4());

    }

    //---Photons
    for(int i=0; i<inTreeBranchGenParticle->GetEntries(); i++){

        GenParticle *photon = (GenParticle*) inTreeBranchGenParticle->At(i);

        if(photon->PID != 22) continue; // pid 22 -> photon 

        //if(photon->Status != 1) continue; // status 41-49 -> ISR, 51-59: FSR (https://pythia.org/latest-manual/ParticleProperties.html)

        int parent = ((GenParticle*) inTreeBranchGenParticle->At(photon->M1))->PID;

        if(abs(parent) == 11) hist_e->Fill(photon->PT);
        if(abs(parent) == 13) hist_mu->Fill(photon->PT);
        if(abs(parent) == 15) hist_tau->Fill(photon->PT);

        if(abs(parent) == 11 || abs(parent) == 13 || abs(parent) == 15) photons.push_back(photon->P4());

    }

    //---Inter-isolation
    bool is_e = true;

    int l = 0;
    for(vector<TLorentzVector> elmus : {pf_electrons, pf_muons}){
      
      for(TLorentzVector elmu : elmus){
        
        l++;

        float inter_iso = 0.;

        int ll = 0;
        for(TLorentzVector lepton : pf_leptons){


          ll++;
          
          if(l != ll && fabs(lepton.DeltaR(elmu)) < .5) inter_iso += lepton.Pt() / elmu.Pt();
          
        }

        if(is_e) hist_iso_e->Fill(inter_iso);
        else hist_iso_mu->Fill(inter_iso);

        for(TLorentzVector photon : photons){

          ll++;
          
          if(fabs(photon.DeltaR(elmu)) < .5) inter_iso += photon.Pt() / elmu.Pt();
          
        }

        if(is_e) hist_iso_e_with_gamma->Fill(inter_iso);
        else hist_iso_mu_with_gamma->Fill(inter_iso);

      }

      is_e = false;

    }

  }

  //--Draw histograms
  TCanvas* can = new TCanvas("can", "", 3000, 2000);
  can->Divide(3, 2);

  can->cd(1);
  gPad->SetLogy();
  hist_e->Draw();

  can->cd(2);
  gPad->SetLogy();
  hist_mu->Draw();

  can->cd(3);
  gPad->SetLogy();
  hist_tau->Draw();

  can->cd(4);
  TLegend* leg_e = new TLegend(.6, .7, .89, .89);
  leg_e->SetBorderSize(0);
  gPad->SetLogy();
  cout<<hist_iso_e->Integral()<<" "<<hist_iso_e_with_gamma->Integral()<<endl;
  hist_iso_e_with_gamma->SetLineColor(kRed);
  TRatioPlot* ratio_e = new TRatioPlot(hist_iso_e, hist_iso_e_with_gamma);
  ratio_e->SetH1DrawOpt("hist");
  ratio_e->SetH2DrawOpt("hist");
  hist_iso_e->SetLineColor(kBlack);
  ratio_e->Draw("nogrid");
  ratio_e->GetLowYaxis()->SetNdivisions(505);
  ratio_e->GetLowerRefYaxis()->SetTitle("ratio");
  ratio_e->GetLowerRefYaxis()->SetRangeUser(.5, 1.5);
  leg_e->AddEntry(hist_iso_e, "without #gamma", "l");
  leg_e->AddEntry(hist_iso_e_with_gamma, "with #gamma", "l");
  leg_e->Draw("same");

  can->cd(5);
  TLegend* leg_mu = new TLegend(.6, .7, .89, .89);
  leg_mu->SetBorderSize(0);
  gPad->SetLogy();
  hist_iso_mu_with_gamma->SetLineColor(kRed);
  TRatioPlot* ratio_mu = new TRatioPlot(hist_iso_mu, hist_iso_mu_with_gamma);
  ratio_mu->SetH1DrawOpt("hist");
  ratio_mu->SetH2DrawOpt("hist");
  hist_iso_mu->SetLineColor(kBlack);
  ratio_mu->Draw("nogrid");
  ratio_mu->GetLowYaxis()->SetNdivisions(505);
  ratio_mu->GetLowerRefYaxis()->SetTitle("ratio");
  ratio_mu->GetLowerRefYaxis()->SetRangeUser(.5, 1.5);
  leg_mu->AddEntry(hist_iso_mu, "without #gamma", "l");
  leg_mu->AddEntry(hist_iso_mu_with_gamma, "with #gamma", "l");
  leg_mu->Draw("same");

  //--Print canvas
  can->Print("test.pdf");

}
